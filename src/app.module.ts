import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './modules/users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CalculatorsModule } from './modules/calculators/calculators.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/nest'), 
    UsersModule,
    CalculatorsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
