import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { CalculatorsService } from './calculators.service';

@Controller('data')
export class CalculatorsController {
    constructor(private readonly calculatorsService: CalculatorsService) {}

    @Post()
    async create(@Body() data) {
        this.calculatorsService.create(data);
    }

    @Post('json/:node/:option')
    async parse(@Param('node') node, @Param('option') option,  @Body() data): Promise<string> {
        return this.calculatorsService.parse(node, option, data);
    }
}