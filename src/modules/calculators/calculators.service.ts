import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class CalculatorsService {

    map = new Map();
    output: string = ``;

    constructor() {}

    async create(data): Promise<any> {
        console.log(JSON.stringify(data));
        axios.post('https://uudjh15y77.execute-api.us-east-2.amazonaws.com/default/call_modelo', JSON.stringify(data));
    }

    async parse(node, option, data) {

        this.output += `And I create map ${option}_${node}_MAP with type Map with the following values\n`;

        if (this.dataType(typeof data, data) == "Sub_List") {
            console.log(data);
            
            let key = `Sub_List_${option}`;
            let value = data;
            let dataType = this.dataType(typeof value, value);
            let row = `${dataType} : ${key}`;
            
            this.addMap(row);
            this.addMap(value);

            let keyRow = `${row} ${(this.map.get(row) > 0) ? ': '+this.map.get(row) : '' }`;
            let valueRow = `${value} ${(this.map.get(value) > 0) ? ': '+this.map.get(value) : ''}`;

            this.output += `\t| ${keyRow} | MAPHEAD |\n`;
            
            if (dataType != "Sub_List" && dataType != "Sub_Map") {
                this.output += `\t| ${valueRow} | ${keyRow} |\n`;
            } else {
                this.subObject(dataType, value, keyRow);
            }
            
        } else {
            for (let key in data) {

                let value = data[key];
                let dataType = this.dataType(typeof value, value);
                let row = `${dataType} : ${key}`;
                
                this.addMap(row);
                this.addMap(value);
    
                let keyRow = `${row} ${(this.map.get(row) > 0) ? ': '+this.map.get(row) : '' }`;
                
                let valueTemp = '';

                if ((dataType == "String")) {
                    if (value.length === 0) {
                        valueTemp = `"${value}"`;
                    } else {
                        valueTemp = value;
                    }
                } else {
                    valueTemp = value;
                }
                
                let valueRow = `${valueTemp} ${(this.map.get(value) > 0) ? ': '+this.map.get(value) : ''}`;
    
                this.output += `\t| ${keyRow} | MAPHEAD |\n`;
    
                if (data.hasOwnProperty(key)) {
                    if (dataType != "Sub_List" && dataType != "Sub_Map") {
                        this.output += `\t| ${valueRow} | ${keyRow} |\n`;
                    } else {
                        this.subObject(dataType, value, keyRow);
                    }
                }
            }
        }

        let result: string = this.output;

        this.map.clear();
        this.output = '';

        return result;
    }

    subObject(dataType, value, key) {
        if (dataType == "Sub_List") {
            this.subList(value, key);
        } else if (dataType == "Sub_Map") {
            this.subMap(value, key);
        } else {
            this.subElement(dataType, value, key);
        }
    }

    subList(data, head) {
        data.forEach(element => {
            this.subObject(this.dataType(typeof element, element), element, head);
        });
    }

    subMap(data, head) {
        let headSubMap = '';
        let rowForSubMap = '';
        let keyRowForSubMap = '';

        if (head.includes('Sub_Map')) {
            headSubMap = head;
            rowForSubMap = head;

            this.addMap(rowForSubMap);
            
            keyRowForSubMap = `${rowForSubMap} ${(this.map.get(rowForSubMap) > 0) ? ': '+this.map.get(rowForSubMap) : '' }`;

        } else {
            headSubMap = head.split(' ').join('').split(':').join('_');
            rowForSubMap = `Sub_Map : ${headSubMap}`;

            this.addMap(rowForSubMap);

            keyRowForSubMap = `${rowForSubMap} ${(this.map.get(rowForSubMap) > 0) ? ': '+this.map.get(rowForSubMap) : '' }`;
            this.output += `\t| ${keyRowForSubMap} | ${head} |\n`;
        }

        

        //this.output += `\t| ${keyRowForSubMap} | ${head} |\n`;

        for (let key in data) {

            let value = data[key];
            let dataType = this.dataType(typeof value, value);
            let row = `${dataType} : ${key}`;

            this.addMap(row);
            this.addMap(value);
            
            let keyRow = `${row} ${(this.map.get(row) > 0) ? ': '+this.map.get(row) : '' }`;
            
            let valueTemp = '';

            if ((dataType == "String")) {
                if (value.length === 0) {
                    valueTemp = `"${value}"`;
                } else {
                    valueTemp = value;
                }
            } else {
                valueTemp = value;
            }
            
            let valueRow = `${valueTemp} ${(this.map.get(value) > 0) ? ': '+this.map.get(value) : ''}`;

            this.output += `\t| ${keyRow} | ${keyRowForSubMap} |\n`;
            if (data.hasOwnProperty(key)) {
                if (dataType != "Sub_List" && dataType != "Sub_Map") {
                    this.output += `\t| ${valueRow} | ${keyRow} |\n`;
                } else {
                    this.subObject(dataType, value, keyRow);
                }
            }
        }
    }

    subElement(dataType, value, head) {
        if (dataType != "Sub_List" && dataType != "Sub_Map") {

            let row = `${dataType} : ${(dataType == "String") ? (value.length === 0) ? `""` : value.split(' ').join('') : value }`;
        
            this.addMap(row);
            this.addMap(value);
    
            let keyRow = `${row} ${(this.map.get(row) > 0) ? ': '+this.map.get(row) : '' }`;

            let valueTemp = '';

            if ((dataType == "String")) {
                if (value.length === 0) {
                    valueTemp = `"${value}"`;
                } else {
                    valueTemp = value;
                }
            } else {
                valueTemp = value;
            }

            let valueRow = `${valueTemp} ${(this.map.get(value) > 0) ? ': '+this.map.get(value) : ''}`;
    
            console.log(row);
            console.log(value);
            
            
            console.log(`\t| ${keyRow} | ${head} |\n`);
            console.log(`\t| ${valueRow} | ${keyRow} |\n`);
            
            this.output += `\t| ${keyRow} | ${head} |\n`;
    
            this.output += `\t| ${valueRow} | ${keyRow} |\n`;
        }
    }

    dataType(type, value) {
        
        switch(type) {
            case "string" : return "String";
            case "number": return "Integer";
            case "boolean": return "Boolean";
            case "object" : {
                if (value instanceof Array ) {
                    return "Sub_List";
                } else {
                    return "Sub_Map";
                }
            }
            default: return "String";
        }
    }


    addMap(key) {

        if (!this.map.has(key)) {
            this.map.set(key, 0);
        } else {
            let count = this.map.get(key);

            this.map.set(key, ++count);
        }
    }

}